var pathArray = window.location.pathname.split('/');
var steps = 0;

var size = 10;
var step = 100 / size;
var i = 5;
var j = 5;

var x = 50;
var y = 50;

function make_id(row, col) {
    return "cellid-" + row + '-' + col;
}

function init() {
    var body = $('<div id="big" />');

    for (var row = 0; row < size; row++) {
        var tr = $('<div id="small" />').appendTo(body);
        for (var col = 0; col < size; col++) {
            var cell = $('<img class="imgItemTen" align="center" src="../../img/pink dust.png"></img>');
            var td = $('<td />').appendTo(tr).append(cell);
            cell.attr('id', make_id(row, col));
            cell.data('row', row);
            cell.data('col', col);
        }
    }

    $('.theGame').append($('<div id="gamePlay"/>').append(body));
    show_image();
}

$(init);

window.onkeyup = function(e) {
    steps = steps + 1;
    var key = e.keyCode ? e.keyCode : e.which;
    if (key == 38) { // up
        y = y - step;
        i = i - 1;
    } else if (key == 40) { // down
        y = y + step;
        i = i + 1;
    } else if (key == 39) { // right
        x = x + step;
        j = j + 1;
    } else if (key == 37) { // left
        x = x - step;
        j = j - 1;
    }
    // border
    if (x < 0) {
        x = 0;
        j = 0;
    }
    if (x >= 100) {
        x = 90;
        j = 9;
    }
    if (y < 0) {
        y = 0;
        i = 0;
    }
    if (y >= 100) {
        y = 90;
        i = 9;
    }
    show_image();
}

function show_image() {
    var pathArray = window.location.pathname.split('/');
    if (pathArray[3] === "il") {
        imgID = "FL45582651";
    } else {
        imgID = "FL49973575";
    }

    $('.imgItemTen').removeClass("selected");
    //document.getElementById('cellid-'+i+'-'+j).style="";
    document.getElementById('cellid-' + i + '-' + j).src = `http://iiif.nli.org.il/IIIFv21/${docID}/pct:` + Math.floor(x) + ',' + Math.floor(y) + ',' + Math.floor(step) + ',' + Math.floor(step) + `/1000,/0/default.jpg`;
    $('#cellid-' + i + '-' + j).addClass("selected");
    $('#cellid-' + i + '-' + j).addClass("changed");
}