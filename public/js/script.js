$("#startGame").click(function() {
    event.preventDefault();
    console.log("Start game", $("#gameType").val(), $("#gameDif").val());
    window.location.href = "/game/" + $("#gameDif").val() + "/" + $("#gameType").val();
});