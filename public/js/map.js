var map;
var distance;
var markers = [];
var pathArray = window.location.pathname.split('/');
var zoomValue = 1;

var geoCodeScriptStr = "/map/" + pathArray[2] + "/" + pathArray[3];
var myMapLocation = {};

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

var myResponse = httpGet(geoCodeScriptStr);
var obj = JSON.parse(myResponse);
var oldReq = obj.SEGMENTS.JAGROOT.RESULT.DOCSET.DOC[Math.floor((Math.random() * 19) + 1)];
console.log("stange", oldReq);
if (oldReq == null){
    location.reload();
}
var title = oldReq.PrimoNMBib.record.display.subject;
var recordid = oldReq.PrimoNMBib.record.control.recordid;
var creator = oldReq.PrimoNMBib.record.display.creator;
var date = oldReq.PrimoNMBib.record.display.creationdate;
var link = oldReq.LINKS.linktorsrc;

var myResponse = httpGet(`http://iiif.nli.org.il/IIIFv21/DOCID/${recordid}/manifest`);


var geoCodeScriptStr = "https://maps.googleapis.com/maps/api/geocode/json?address=" + title + "&key=AIzaSyAV_Apbxn8B8rxQeu_GvNPiQJgR1CMGEzA&callback=myLocation";

var results = httpGet(geoCodeScriptStr);
var resultsJSON = JSON.parse(results);
console.log(resultsJSON);
if (resultsJSON.results.length == 0) {
    //location.reload();
} else {
    var lat1 = resultsJSON.results[0].geometry.location.lat;
    var lon1 = resultsJSON.results[0].geometry.location.lng;
    myMapLocation = { lat: lat1, lng: lon1 };
}

var obj = JSON.parse(myResponse);
console.log(obj.sequences[0], oldReq, title);
var docID = obj.sequences[0].canvases[0].images[0]["@id"];

function myMap() {
    var pathArray = window.location.pathname.split('/');

    var zoomValue = 1;
    if (pathArray[3] === "il") {
        zoomValue = 7;
    }

    var mapProp = {
        center: new google.maps.LatLng(31.771959, 35.217018),
        zoom: zoomValue,
    };

    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);


    google.maps.event.addListener(map, 'click', function(e) {
        var marker = new google.maps.Marker({
            position: e.latLng,
            map: map,
            title: 'This is your chosen location!'
        });

        var marker2 = new google.maps.Marker({
            position: myMapLocation,
            map: map,
            title: 'This is the correct location!'
        });
        clickedLocation = e.latLng;
        //alert(clickedLocation.lat);
        //alert(clickedLocation.lng());
        distance = getDistanceFromLatLonInKm(clickedLocation.lat(), clickedLocation.lng(), myMapLocation.lat, myMapLocation.lng);
        //alert((1 / (steps * distance.toFixed(3))) * 100);
        $("#distance").text(distance.toFixed(0));
        $("#steps").text(steps);
        $("#creator").text(creator);
        $("#date").text(date);
        console.log(link);
        $("#link").attr("href", link);

        $("#myModal").modal();
        var tempDictLoct = { lat: clickedLocation.lat, lng: clickedLocation.lng }
        var lineCoords = [tempDictLoct, myMapLocation];
        var flightPath = new google.maps.Polyline({
            path: lineCoords,
            geodesic: true,
            strokeColor: '#FF0001',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        //(1 / steps * distance.toFixed(3)) * 100
    });
}



function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

// Adds a marker to the map and push to the array.
function addMarker(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    //marker.setMap(map);
}