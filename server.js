var express = require("express");
var bodyParser = require("body-parser");
var morgan = require('morgan');
var request = require('request');


var app = express();

// Configuration
var port = process.env.PORT || 3000;

app.use(express.static('public'));

//var mapsURL = "http://primo.nli.org.il/PrimoWebServices/xservice/search/brief?institution=NNL&loc=local,scope:(NNL_MAPS_JER)&query=lsr41,exact,yes&indx=1&bulkSize=10&json=true&query=dr_s,exact,16500101&query=dr_e,exact,20171231&query=subject,contains,NOT Tel Aviv";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/main.html');
});

app.get('/game/:diff/:type/', function(req, res) {
	console.log(req.params.diff, req.params.type);
    res.sendFile(__dirname + '/public/game.html');
});

app.get('/allMaps', function(req, res) {
    request(allMapsUrl, function(error, response, body) {
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        //console.log('body:', body);
        res.send(body);
    });
});

app.get('/isrealMaps', function(req, res) {
    request(isrealMapsUrl, function(error, response, body) {
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        //console.log('body:', body);
        res.send(body);
    });
});

app.get('/map/:diff/:type', function(req, res) {
    console.log(req.params.diff, req.params.type);
    var startTime = "";
    var endTime = "";

    if (req.params.diff == "1") {
        startTime = "19500101";
        endTime = "20171231";
    }
    if (req.params.diff == "2") {
        startTime = "16500101";
        endTime = "19000101";
    }
    if (req.params.diff == "3") {
        startTime = "14750101";
        endTime = "16500101";
    }
    var randomInt = Math.floor((Math.random() * 100) + 1);
    var allMapsUrl = `http://primo.nli.org.il/PrimoWebServices/xservice/search/brief?institution=NNL&loc=local,scope:(NNL_MAPS_JER)&query=lsr41,exact,yes&bulkSize=20&json=true&query=dr_s,exact,${startTime}&query=dr_e,exact,${endTime}&query_exc=sub,contains,Israel&indx=` + randomInt;
    var isrealMapsUrl = `http://primo.nli.org.il/PrimoWebServices/xservice/search/brief?institution=NNL&loc=local,scope:(NNL_MAPS_JER)&query=lsr41,exact,yes&bulkSize=20&json=true&query=dr_s,exact,${startTime}&query=dr_e,exact,${endTime}&query=sub,contains,Israel&indx=` + randomInt + "&query_exc=sub,contains,Eretz";
    console.log(allMapsUrl);
    if (req.params.type === "il") {

        request(isrealMapsUrl, function(error, response, body) {
            res.send(body);
        });
    } else {
        request(allMapsUrl, function(error, response, body) {
            res.send(body);
        });
    }
});



app.listen(port);
console.log('Magic happens at http://localhost:' + port);